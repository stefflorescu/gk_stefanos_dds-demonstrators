.. _MQTT:

=========================================
MQTT: Message Queuing Telemetry Transport
=========================================

   MQTT is an ISO standard publish-subscribe-based messaging protocol. MQTT works on top of TCP/IP and is widely used due to
   it being lightweight and simple in use. MQTT is especially popular in iot applications with low bandwidth network connections.
   Its principle is very simple, there is a central server called the message broker. A publisher sends a message to the broker on a specific topic. 
   The broker then sends this message to every device or process that subscribed to this topic. 
   
Install
=======

Broker
------

There are two parts to installing MQTT, there is the broker and the client.
Mosquitto is one such broker developed by the eclipse foundation and is easily installed under debian linux with: 

.. code-block:: python
   
   sudo apt-get install mosquitto

For debugging purposes you might also want to install the cmd line tool:

.. code-block:: python
   
   sudo apt-get install mosquitto-clients
   
Python
------

Python uses the paho-mqtt library and can be installed with the following:

.. code-block:: python
   
   pip install paho-mqtt
   
