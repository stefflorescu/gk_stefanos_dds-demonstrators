.. _RoundTrip_Talk_like_pi:

RoundTrip
=========

To test the different ways of communication, what is called a roundtrip is used. 
In a round trip four different processes or systems send messages to one another.
In the round trip there is one master and three hubs it looks a little like this:

.. uml::

   @startuml
   master -right-|> hub1 : message
   hub1 -down-|> hub2 : message
   hub2 -left-|> hub3 : message
   hub3 -up-|> master : message
   
   
   @enduml
   
In a time diagram it looks a little like this:

.. uml::

   @startuml
   
   master -> hub1    : message[1]
   hub1   -> hub2    : message[1]
   hub2   -> hub3    : message[1]
   hub3   -> master  : message[1]
   
   master -> hub1    : message[2]
   hub1   -> hub2    : message[2]
   hub2   -> hub3    : message[2]
   hub3   -> master  : message[2]
   
   master -> hub1    : message[3]
   hub1   -> hub2    : message[3]
   hub2   -> hub3    : message[3]
   hub3   -> master  : message[3]
   
   @enduml

In the example above the message makes three roundtrips. Of course in practice this will be thousands of round trips to accurately determine the amount of time it takes to go around. 
As shown every time the message passes the master its contents is increased by one. This way the master can calculate how many times a message passes every second or how long it takes for the message to take a round trip.
Another way of measuring the methods of communication is sending a bunch of messages and see how long it takes for them to come back. That looks something like: 

.. uml::

   @startuml
   
   master   -> hub1     : message[1]
   master   -> hub1     : message[2]
   master   -> hub1     : message[3]
   
   hub1     -> hub2     : message[1]
   hub1     -> hub2     : message[2]
   hub1     -> hub2     : message[3]
   
   hub2     -> hub3     : message[1]
   hub2     -> hub3     : message[2]
   hub2     -> hub3     : message[3]
   
   hub3     -> master   : message[1]
   hub3     -> master   : message[2]
   hub3     -> master   : message[3]
   
   @enduml
   
Both of these tests are interesting as the first test migth give an idea whether the communication is fast enough for example in a controlled system. The second is interesting as in practice there may be many
systems communication many different things and this simulates that situation. 

Bandwidth
---------

**Sending a bunch of messages and time how long it takes for them to come back**

similar to the Roundtrip there is a different way of testing the speed of a connection. This is also results in a time spent per message.
In this method the master sends a whole bunch of messages and then waits until they all come back. Dividing the time it took of the 
messages to come back by the amount of messages also gives an amount of time. 
   
