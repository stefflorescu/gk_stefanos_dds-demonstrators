DDS
===

DDS(Dynamic Data Distribution) 


Running DDS
***********

Like for MQTT and ZMQ, DDS also needs some preparations to use: 

   * ZMQ is installed as per the IPSC manual
   * you have four raspberries configured as per the setup chapter
   * the repository has been downloaded

It is possible to make a round trip using the python scripts. This can be done like this::
 
   python3 <techpush-sourcedir>/demonstrators/RoundTrip/Python/DDShub.py Master_Out hub1
   python3 <techpush-sourcedir>/demonstrators/RoundTrip/Python/DDShub.py hub1 hub2
   python3 <techpush-sourcedir>/demonstrators/RoundTrip/Python/DDShub.py hub2 hub3
   python3 <techpush-sourcedir>/demonstrators/RoundTrip/Python/DDShub.py hub3 Master_in
   python3 <techpush-sourcedir>/demonstrators/RoundTrip/Python/RoundTrip.py
   
These commands can be run on the same or seperate systems and they still work. DDS will work out how to connect them, of course the systems do have to be on the same
network. 

In short what happens here is that first three DDS-hubs are started. the first argument given is the topic the hub is listening to for incoming messages,
the second input is the topic to which the received message will be sent. The master functions basicly the same way as the hub with only two differences:
it adds 1 to the message each time it passes and some time after it started it sends the first message.


