.. _RoundtripC:


Roundtrip and Flood Demonstrators in C
--------------------------------------

The Roundtrip demonstrator has two different experiments. The first experiment sets up one main master hub and three hubs. The master hub sends the value ``1``
and starts measuring time upon sending the message. This message travels from hub to hub with each hub adding 1 until it returns to the master. Upon arrival of the modified
integer from the third hub at the master, the clock is stopped and the time it took for the roundtrip is displayed.

Similarly to the simple roundtrip experiment, the flood experiment has the same setup of master and three hubs. The only difference is that the master now sends a bunch of messages at the same time.
The hubs read the messages one by one, process them (by adding 1) and pass them on. The master will start measuring time from the moment he sends his first message and up to the moment
he receives the last message from the last hub.

More information on these demonstrators can be found in the Backlog.

Building and running the executables
""""""""""""""""""""""""""""""""""""

Make sure your local repo is up to date with the `HighTech-nl repo <https://bitbucket.org/HighTech-nl/dds-demonstrators/src/master/>`_ and move into the directory ``/dds-demonstrators/src/demonstrators/RoundTrip/C``::

   $ mkdir build
   $ cd build
   $ cmake ..
   $ cmake --build .

Also, after the ``build`` directory has been initialized using the ``cmake ..`` command, the two scripts ``run_roundtrip.sh`` and ``run_flood.sh``
can be used to compile the executables and run the experiments. These two scripts are similar with the only differences being that they call different
executables. For example the ``run_roundtrip.sh`` includes the following:

.. code-block:: Console

   #!/bin/bash

   cmake --build $PWD/build/
   echo "Deleting old log file."
   rm $PWD/roundtrip.log

   echo "Running Roundtrip Demonstrator...press ^C to exit"
   ./build/Hub_roundtrip topic_2 topic_3 &
   ./build/Hub_roundtrip topic_3 topic_4 &
   ./build/Hub_roundtrip topic_4 topic_1 &
   ./build/Master_roundtrip topic_1 topic_2 > roundtrip.log

The master will output all his measurements into the ``.log`` file in each respective case. Furthermore, the first argument passed to each Hub/Master executable is
the topic name on which that entity will be listening on, while the second argument is the topic on which that entity will write to. In order to see the output of the experiment
simply open the respective ``.log`` file.


.. note::

   The Master/Hub can be executed on different nodes, such as Raspberry Pis and PCs. See :ref:`rpis` for details on how to connect to the Raspberry Pis.

.. note::

   In the case of the flood experiment, the number of samples the Master will send can be defined in the ``Master_flood.c`` file by changing the ``#define FLOOD_SAMPLES`` number.
