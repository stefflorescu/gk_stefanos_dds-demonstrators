.. _setupDDSPython:

Setup Guide for Cyclone DDS Python API
--------------------------------------

Geting Started
""""""""""""""

The Python DDS binding depends on:

* jsonpickle 
* CycloneDDS
 

Installation
""""""""""""

To install jsonpickle via Pip on Linux/Raspberry Pi:: 

    $ pip3 install jsonpickle     

To download, build and install the Python DDS binding on Linux/Raspberry Pi run::

    $ git clone https://github.com/atolab/cdds-python
    $ cd python-cdds
    $ ./configure
    $ python3 setup.py install

Since two library files are required for the Python binding after executing the first ``./configure`` go into ``/cdds-python/bit/build/`` 
and copy the ``libddstubs.so`` into ``/usr/local/lib/``.

Edit the following line in the ``configure`` script to be as follows:

.. code-block:: Console

   BIT_LIB=libddsc.so 

Also edit the following two lines in ``CMakeLists.txt`` to look as follows:

.. code-block:: Console

   add_library(ddsc SHARED dds_stubs.c)
   target_link_libraries(ddsc ddsbit CycloneDDS::ddsc) 

Run again::    

    $ cd python-cdds
    $ ./configure
    $ python3 setup.py install

Finally copy the ``libddsc.so`` file from ``/cdds-python/bit/build/`` into ``/usr/local/lib/``.  


.. warning:: 
   
   When executing ``./configure`` an error might occur stating that ``idlc_generate`` was not found. 
   Simply edit the ``CMakeLists.txt`` inside ``/cdds-python/bit/`` to look for the Cyclone DDS package in the correct location:
   
   .. code-block:: Console
       
      find_package(CycloneDDS REQUIRED COMPONENTS idlc PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../../../..")
      
.. warning:: 
   
   Running the command ``python3 setup.py install`` requires internet connection in order to configure the jsonpickle package. 

Links
"""""

* Cyclone DDS Python API: https://github.com/atolab/cdds-python
