.. _Team_hurricane:

Team Hurricane
==============

.. image:: hurricane.jpg
   :width: 400
   :align: center


.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    setupCycloneDDS
    setupDDSPython
    DDSguide
    MatrixBoardC
    RoundtripC
    rpis
    plantuml
    keep_repo_updated
    merging_git_repos
    migrating_mercurial_to_git
