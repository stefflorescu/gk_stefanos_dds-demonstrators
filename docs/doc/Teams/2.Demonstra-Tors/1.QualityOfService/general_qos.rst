.. _general_qos:

General findings
----------------

:authors: Joost Baars
:date: Feb 2020

Description
"""""""""""
This page shows the general findings regarding the QoS policies.

Performance
"""""""""""

The dds_write function is around twice as fast with a static message (a message that stays the same) compared to a message that is changing after each write.
A struct was sent using DDS containing 3 parameters. It did not seem to matter how much data was changed in the struct. 
If the struct was changed (even only one byte), the execution time of the write function would be around twice as slow. 
The execution time of one write action was measured in this setup (the average of 10k measurements). The average result was around 8500 nanoseconds for a static message and around 16000 nanoseconds for a changing message.
These measurements are not reliable though but it shows that there is a clear difference between the two configurations. 

CycloneDDS missing features
"""""""""""""""""""""""""""
 * The DDS extension for the QoS Database policy
 * Asynchronous publisher does not seem to be implemented (possibly done automatically for you)