.. _qos_explanation:

Quality Of Service
------------------

:authors: Joost Baars
:date: Feb 2020

Description
"""""""""""
The Quality of Service (QoS) are a set of configurable parameters that control the behavior of a DDS system (how and when data is distributed between applications). Some of the parameters can alter the resource consumption, fault tolerance or the reliability of the communication.

Each entity (reader, writer, publisher, subscriber, topic and participant) of DDS has associated QoS policies to it. Some policies are only for one entity, others can be used on multiple entities. 

More information about the QoS policies as well as a list of existing DDS policies can be found in the `Links`_.

QoS findings
""""""""""""
This list contains the findings regarding the QoS policies. 

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    general_qos
    

Links
"""""
* General information regarding QoS within DDS: https://community.rti.com/glossary/qos
* List of all QoS policies within DDS: https://community.rti.com/rti-doc/500/ndds.5.0.0/doc/pdf/RTI_CoreLibrariesAndUtilities_QoS_Reference_Guide.pdf
* List of all QoS policies (less information but with hyperlinks): https://community.rti.com/static/documentation/connext-dds/5.2.0/doc/manuals/connext_dds/html_files/RTI_ConnextDDS_CoreLibraries_UsersManual/Content/UsersManual/QosPolicies.htm
* Some examples given by RTI: https://community.rti.com/static/documentation/connext-dds/5.2.0/doc/manuals/connext_dds/html_files/RTI_ConnextDDS_CoreLibraries_UsersManual/Content/UsersManual/ControllingBehavior_withQoS.htm