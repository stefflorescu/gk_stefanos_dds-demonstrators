.. _Team_demonstra-tors:

Team Demonstra-Tors
===================

:authors: Sam Laan, Furkan Ali Yurdakul, Bendeguz Toth and Joost Baars
:date: Feb 2020

.. image:: tor.png
   :width: 600
   :align: center

These team pages contain our **findings** during the project. 

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    idl_explanation
    */index
