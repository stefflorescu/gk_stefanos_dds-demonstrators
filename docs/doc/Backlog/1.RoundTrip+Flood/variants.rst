.. (C) ALbert Mietus, Sogeti, 2019

Variants
========

.. req:: Python RoundTrip
   :id: RT_PY_01c
   :links: RT_001
   :status: import

   Implemented the :need:`RT_001` Demonstrator in python; using the CycloneDDS stack and the *“asis”* “atolab” python binding.

.. req:: C++ RoundTrip
   :id: RT_Cpp_01c
   :links: RT_001
   :status: import

   Implemented the :need:`RT_001` Demonstrator in C++; using the CycloneDDS stack.

.. req:: C RoundTrip
   :id: RT_C_01c
   :links: RT_001
   :status: import

   Implemented the :need:`RT_001` Demonstrator in plan-old-C; using the CycloneDDS stack.

.. req:: Python Flood
   :id: F_PY_01c
   :links: F_001
   :status: import

   Implemented the :need:`F_001` Demonstrator in python; using the CycloneDDS stack and the *“asis”* “atolab” python binding.

.. req:: C++ Flood
   :id: F_Cpp_01c
   :links: F_001
   :status: import

   Implemented the :need:`F_001` Demonstrator in C++; using the CycloneDDS stack.

.. req:: C Flood
   :id: F_C_01c
   :links: F_001
   :status: import

   Implemented the :need:`F_001` Demonstrator in plan-old-C; using the CycloneDDS stack.


Notes
-----

The “atolab” python-binding do work for the :need:`RT_001` and :need:`F_001` Demonstrators; but there are some know
issues. It looks like this library is kind of *PoC*:

- Python-objects are (typically) serialised into json and transfers as strings. This works for python-to-python, but not
  with other languages. It also not according to the “wire specifications”
- This Python-API does not expose the full (C/Conceptual) API. More advantage DDS-features are impossible.
- The Python interface is not very pythonic
- Note: There is not official pythonic-API.

For the basic-Demonstrator this is not an issue. For the long run, a better, completer and more pythonic-API would be
welcome. Then the new  (official) Java- and C++ style interfaces are a better start. Its is future work.


Links
-----

* CycloneDDS sources are on github: https://github.com/eclipse/cyclonedds
* The atolab-python binding https://github.com/atolab/python-cdds>
