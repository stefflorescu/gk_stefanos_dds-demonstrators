PingPong
--------
* First start Ping, then Pong; in 2 terminals
* You have about 6 seconds between -- the SLEEP is hardcoded
* Stop (both) by killing (^C) them
* Ping and Pong may run on different systems (or on the same).
* Same network segment

or go to RoundTrip folder and look at the readme there for setting up a roundtrip
