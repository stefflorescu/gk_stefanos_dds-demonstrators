RoundTrip
---------

Description
"""""""""""
To test the different ways of communication, what is called a roundtrip is used. 
In a round trip, different processes or systems send messages to one another. 
There can only be one master, but there can be an infinite amount of hubs. 
The master and hubs together form a circle, where one message loops through all the devices.

The round trip is used for measuring the time needed to send and receive a message on a network. 
The time between each device can easily be measured. 

A roundtrip with one master and three hubs looks like this:

.. uml::

   @startuml
   master -right-|> hub1 : message
   hub1 -down-|> hub2 : message
   hub2 -left-|> hub3 : message
   hub3 -up-|> master : message
   
   
   @enduml


Implementation
""""""""""""""
There are different implementations of the round trip. 
A short description is made for the execution of a particular implementation. 
This description contains information about how the implementation can be compiled and executed. 

This list contains the different implementations:

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    cplusplus
    python
