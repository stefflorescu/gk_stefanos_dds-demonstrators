.. _cplusplus:

Round trip in C++
-----------------

:authors: Joost Baars
:date: Mar 2020

Dependencies
""""""""""""
The dependencies necessary for succesful compilation of the round trip application in C++ are the following:
 * CMake
 * C++17 compiler (Gcc 7+ for example)
 * CycloneDDS library


Configuring
"""""""""""
First go to the roundtrip C++ directory. 

The C++ round trip implementation contains an implementation for the round trip with the use of callbacks or the use of of polling and reading.
One of these implementations can be chosen before building the project. 

The implementation can be configured by altering the following line in the ``CMakeLists.txt``:

.. code-block:: bash

	set(PROJECT roundtrip_read)

This line can be found at the top of the ``CMakeLists`` file. The ``roundtrip_read`` can be changed to ``roundtrip_callback`` for the callback implementation.

Building
""""""""
First go to the round trip C++ directory. Execute the following commands:

  .. code-block:: bash

	mkdir build && cd build
	cmake ..
	make
	
These commands compile the code and create the executable.

Execution
"""""""""
The application must be executed using various parameters.
The application can be executed using the following command:

  .. code-block:: bash

	./RoundTrip <device ID> <number of devices> <total round trips>

The following example starts 4 nodes for the round trip. 
The slaves of the round trip are started in the background. 
Only the master is started in the foreground in this example.
Each device pings a total of 1000 times.  Therefore, there are 1000 round trips. 

  .. code-block:: bash

	./RoundTrip 2 4 1000 & ./RoundTrip 3 4 1000 & ./RoundTrip 4 4 1000 &
	./RoundTrip 1 4 1000
	
A more thorough description of the parameters can be found when executing the application with no parameters.

.. note:: Execution of the program

   The roundtrip is initiated by the device with <device ID> = 1. Therefore, <device ID> = 1 should always be started after the other ID's are started.
   
   The devices added with <device ID> must be an increment of the previous one. This function does not dynamically search for the next device!
   So if there are three devices, device 2 and 3 should be started first. Afterwards, device 1 can be started. 
   
   The <total round trips> parameter should be the same for each application.

Implementation
""""""""""""""
Each round trip application creates a participant containing a reader and a writer.
The writer writes to the topic of the ID above it. So applications with ID = 1 writes to the application with ID = 2.
The reader reads from its own topic. These topics have the name "roundtrip" with the ID behind it. 
So the topic with ID = 1 is "roundtrip1".

The application with ID = 1 initiates the roundtrip. Therefore, it starts with writing to topic "roundtrip2". 
The application with ID = 2 then receives the message of the application with ID = 1, and sends a message to the next application.

Read
~~~~
The read implementation continuously executes the ``dds_take()`` function to get the latest message of its own topic. 
``dds_take()`` is used so the message is directly removed from DDS and so it won't be read again. 
When a message is received, the application uses ``dds_write()`` for writing to the next application.

Callback
~~~~~~~~
The callback implementation uses a callback function for receiving the latest message and writing to the next application. 
This implementation sleeps until data is received on the readers topic. 
When data is received, a callback function is executed where ``dds_write()`` writes to the next application in the round trip.
