.. _python:

Round trip in Python
--------------------

Use DDShub.py to setup either a flood or roundtrip setup (see run_L3.sh for an example).
To run the flood example::

   ./run_L3.sh
   ./Flood.py

To run the round trip example::

   ./run_L3.sh
   ./RoundTrip.py
