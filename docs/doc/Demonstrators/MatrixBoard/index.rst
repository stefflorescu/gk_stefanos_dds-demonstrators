#######################################
MatrixBoard - DDS traffic in one domain
#######################################

For the matrix board different implementations have been created.


***************************
Proof of concept 1 - python
***************************

This implementation uses a single topic to which all nodes write and read. On detection of another reader it is determined whether the information for this reader is relevant.

.. note:: For this implementation to run the following dependency needs to be installed: pynput
          This dependency is used for the keyboard strokes to be handled during the execution.

.. code-block:: bash

   pip3 install pynput

.. note:: Be aware to set the display settings, these are required

.. code-block:: bash

   cd <name of the repo>/src/demonstrators/MatrixBoard/Python/poc1
   python3 node.py <location marker>

for <location marker> 17.1, 17.2, etc. can be used

***************************
Proof of concept 2 - Python
***************************

This implemention uses more than one topic. Each node writes to its own topic and determines if there is a next topic to read from. The information of the two next topics will be used. In case one of the next topics is not a preferred topic and a preferred one becomes available, the preferred one will replace the other.

Usage:

.. code-block:: bash

   python3 MatrixBoard.py <topic number>

The <topic number> must be a value: 1..10

