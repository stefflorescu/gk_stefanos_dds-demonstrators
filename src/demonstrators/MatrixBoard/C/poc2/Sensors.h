#ifndef SENSORS_H
#define SENSORS_H

#include "dds/dds.h"

/* Define max and min displayed speed */
#define MAX_SPEED  120
#define LOW_SPEED  50

int create_traffic();
int adapt_speed(int32_t traffic);
 
#endif