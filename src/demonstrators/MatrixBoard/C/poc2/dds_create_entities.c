#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dds/dds.h"
#include "MBCData.h"
#include "dds_create_entities.h"

/* This function creates a topic given the MBC's number and the participant and returns the topic's handle. */
dds_entity_t create_topic(int MBC_number, dds_entity_t participant){

  dds_entity_t topic;
  char array1[10];
  char topic_str[10]= "Topic";
  const char *name;

  sprintf(array1,"%d",MBC_number);
  strcat(topic_str,array1);
  name = topic_str;
  topic = dds_create_topic (participant, &MBCData_Msg_desc, name, NULL, NULL);
  if (topic< 0){
    DDS_FATAL("dds_create_topic: %s\n", dds_strretcode(-topic));
  }
  return topic;
}

/* This function creates a reader on a given topic and returns the reader's handle. */
dds_entity_t create_reader(dds_entity_t participant, dds_entity_t topic, dds_listener_t *listener){

  dds_qos_t *qos;
  dds_entity_t reader;

  qos = dds_create_qos ();
  dds_qset_reliability (qos, DDS_RELIABILITY_RELIABLE, DDS_SECS (10));
  reader = dds_create_reader (participant, topic, NULL,listener);
  if (reader < 0){
    DDS_FATAL("dds_create_reader: %s\n", dds_strretcode(-reader));
  }
  dds_delete_qos(qos);
  return reader;
}

/* This function creates a writer on a given topic and returns the writer's handle. */
dds_entity_t create_writer(dds_entity_t participant , dds_entity_t topic){

  dds_entity_t writer;

  writer = dds_create_writer (participant, topic, NULL, NULL);
  if (writer < 0){
    DDS_FATAL("dds_create_write: %s\n", dds_strretcode(-writer));
  }
  return writer;
}