#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include "dds/dds.h"
#include "MBCData.h"
#include "dds_create_entities.h"
#include "Sensors.h"
#include "dds_read_write_topic.h"

/* Define max number of MBCs. */
#define MAX_SEARCH 100

volatile sig_atomic_t stop;

/* Function to handle keyboard interrupt (Ctrl + C). */
void inthand(int signum){
  stop = 1;
}

static void on_liveliness_change(dds_entity_t reader, const dds_liveliness_changed_status_t status, void *arg){
  (void)arg;
  static int count = 0;
  count += status.alive_count_change;
  printf("Current liveliness %d\n",count);
}

int main (int argc, char *argv[]){

  dds_entity_t participant;
  dds_entity_t topics[MAX_SEARCH];
  dds_entity_t readers[MAX_SEARCH];
  dds_return_t rc_read1;
  dds_return_t rc_read2;
  dds_entity_t topic_write;
  dds_entity_t writer;
  dds_return_t rc_write;
  dds_publication_matched_status_t status_write;
  dds_subscription_matched_status_t status_read1;
  dds_subscription_matched_status_t status_read2;
  dds_liveliness_changed_status_t status_alive;
  dds_listener_t *listener;

  int MBC_id;
  int error_write;
  int error_read1;
  int error_read2;
  int i;
  int j;

  char array_convert[10];
  char topic_id[10]= "Topic";

  srand(time(0)); //Initialize random seed for traffic rng
  signal(SIGINT, inthand);

  if( argc == 2 ){
    MBC_id = atoi(argv[1]);
    printf("Matrix board with ID number %d is online\n", MBC_id);
  }
  else if( argc > 2 ){
    printf("Too many arguments supplied.\n");
    return EXIT_FAILURE;
  }
  else{
    printf("One argument expected.\n");
    return EXIT_FAILURE;
  }

  /* Create a Participant.*/
  participant = dds_create_participant (DDS_DOMAIN_DEFAULT, NULL, NULL);
  if (participant < 0){
    DDS_FATAL("dds_create_participant: %s\n", dds_strretcode(-participant));
  }

  /* Create MBC's own write Topic. */
  topic_write = create_topic (MBC_id, participant);

  /* Create MBC's own Writer. */
  writer = create_writer (participant, topic_write);

  /* Build Topic sender id. */
  sprintf(array_convert,"%d",MBC_id);
  strcat(topic_id," ");
  strcat(topic_id,array_convert);

  /* Create Listener Callback for liveliness changes. */

  listener = dds_create_listener(NULL);
  dds_lset_liveliness_changed(listener, on_liveliness_change);

  /* Create readers array on all topics. */
  for (i = MBC_id + 1; i < MAX_SEARCH; i++){
    topics[i] = create_topic (i, participant);
    readers[i] = create_reader (participant, topics[i], listener);
  }
  dds_delete_listener(listener);

  while (!stop){
    rc_write = dds_get_publication_matched_status (writer, &status_write);
    /* Check if we have active readers on our write topic. */
    if (status_write.current_count){
      error_write = write_topic(writer, rc_write, MBC_id, topic_id);
    }
    for (i = MBC_id + 1; i < MAX_SEARCH-1; i++){
      rc_read1 = dds_get_subscription_matched_status (readers[i], &status_read1);
      if (status_read1.current_count){
        error_read1 = read_topic(readers[i],rc_read1,MBC_id);
        for (j = i+1;j < MAX_SEARCH-1; j++){
          rc_read2 = dds_get_subscription_matched_status (readers[j], &status_read2);
          if (status_read2.current_count){
            error_read2 = read_topic(readers[j],rc_read2,MBC_id);
            break;
          }
        }
      }
      if (status_read1.current_count && status_read2.current_count){
        break;
      }
    }
    dds_sleepfor (DDS_MSECS (1000));
  }

  /* Deleting the participant will delete all its children recursively as well. */
  rc_read1 = dds_delete (participant);
  if (rc_read1 != DDS_RETCODE_OK){
    DDS_FATAL("dds_delete: %s\n", dds_strretcode(-rc_read1));
  }
  printf("\nExiting safely >> Bye Bye from Matrix Board %d \n", MBC_id);
  return EXIT_SUCCESS;
}
