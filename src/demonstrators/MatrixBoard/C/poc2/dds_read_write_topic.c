#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dds/dds.h"
#include "MBCData.h"
#include "dds_create_entities.h"
#include "Sensors.h"
#include "dds_read_write_topic.h"

/* The following function writes the topic using dds_write and prints the contents of the write action. */
int write_topic(dds_entity_t writer, dds_return_t rc_write, int MBC_id,  char topic_id[]){

  MBCData_Msg msg;
  /* Create a message to write. */
  msg.userID = MBC_id;
  msg.traffic = create_traffic();
  msg.message = topic_id;

  printf ("=== [MBC %d]  Writing : " , MBC_id);
  printf ("Traffic %"PRId32" to %s\n", msg.traffic, msg.message);

  rc_write = dds_write (writer, &msg);
  if (rc_write != DDS_RETCODE_OK){
    DDS_FATAL("dds_write: %s\n", dds_strretcode(-rc_write));
  }
  return rc_write;
}

/* The following function reads the topic using dds_take and prints the contents of the read action. */
int read_topic(dds_entity_t reader, dds_return_t rc_read, int MBC_id){

  MBCData_Msg *msg;
  void *samples[MAX_SAMPLES];
  dds_sample_info_t infos[MAX_SAMPLES];

  samples[0] = MBCData_Msg__alloc ();
  rc_read = dds_take (reader, samples, infos, MAX_SAMPLES, MAX_SAMPLES);
  if (rc_read < 0){
    DDS_FATAL("dds_take: %s\n", dds_strretcode(-rc_read));
  }
  /* Check if we read some data and it is valid. */
  if ((rc_read > 0) && (infos[0].valid_data))
  {
    /* Print Message. */
    msg = (MBCData_Msg*) samples[0];
    printf ("=== [MBC %d] Received : " , MBC_id);
    printf ("Traffic from MBC %"PRId32": %"PRId32" on %s\n", msg->userID,msg->traffic, msg->message);

    adapt_speed(msg->traffic);
    fflush (stdout);
  }
  MBCData_Msg_free (samples[0], DDS_FREE_ALL);
  return rc_read;
}
