#ifndef DDS_READ_WRITE_TOPIC_H
#define DDS_READ_WRITE_TOPIC_H

/* Define number of Samples read. */
#define MAX_SAMPLES 1

int write_topic(dds_entity_t writer, dds_return_t rc_write, int MBC_id,  char topic_id[]);
int read_topic(dds_entity_t reader, dds_return_t rc_read, int MBC_id);

#endif