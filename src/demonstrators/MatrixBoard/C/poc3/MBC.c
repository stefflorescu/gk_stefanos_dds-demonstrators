#include "MBCData.h"
#include "dds/dds.h"
#include "dds_create_entities.h"
#include "dds_create_membership_entities.h"
#include "dds_read_write_membership_topic.h"
#include "dds_read_write_topic.h"
#include "hash.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Global variables used for callback functions. */
bool add_mbc_liveliness = false;
bool remove_mbc_data = false;
volatile sig_atomic_t stop;

typedef enum
{
  SEARCHING,
  READING
} State;

/* Functions forward declarations */
void inthand(int signum);
static void on_liveliness_change(dds_entity_t reader, const dds_liveliness_changed_status_t status, void* arg);
bool insert_MBC(dds_entity_t reader_membership, dds_entity_t writer_membership, int32_t MBC_id_32);
void delete_MBC(dds_instance_handle_t last_publication_handle);

int main(int argc, char* argv[])
{
  /* Declare Participant. */
  dds_entity_t participant;

  /* Declare Writers. */
  dds_entity_t writer;
  dds_entity_t writer_membership;

  /* Declare Topics. */
  dds_entity_t topic_write;
  dds_entity_t topic_read1;
  dds_entity_t topic_read2;
  dds_entity_t topic_membership;

  /* Declare readers */
  dds_entity_t reader1;
  dds_entity_t reader2;
  dds_entity_t reader_membership;

  /* Declare error returns. */
  dds_return_t rc_write;
  dds_return_t rc_write_membership;
  dds_return_t rc_read1;
  dds_return_t rc_read2;

  /* Declare status variables. */
  dds_publication_matched_status_t status_write;
  dds_liveliness_changed_status_t status_members_alive;
  dds_instance_handle_t writer_handle;
  dds_listener_t* listener;

  int32_t min1 = 0;
  int32_t min2 = 0;
  int32_t MBC_id_32;
  int MBC_id;
  int error_read1;
  int error_read2;
  int i;
  int j;
  int total_mbc_added;
  bool return_status_new_mbc;
  State read1 = SEARCHING;
  State read2 = SEARCHING;
  char array_convert[10];
  char topic_id[10] = "Topic";
  struct DataItem* item;
  struct info_member add_member;

  srand(time(0)); // Initialize random seed for traffic rng
  signal(SIGINT, inthand);

  if (argc == 2)
  {
    MBC_id = atoi(argv[1]);
    MBC_id_32 = atoi(argv[1]);
    printf("Matrix board with ID number %" PRId32 " is online\n", MBC_id_32);
  }
  else if (argc > 2)
  {
    printf("Too many arguments supplied.\n");
    return EXIT_FAILURE;
  }
  else
  {
    printf("One argument expected.\n");
    return EXIT_FAILURE;
  }

  /* Create a Participant. */
  participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
  if (participant < 0)
  {
    DDS_FATAL("dds_create_participant: %s\n", dds_strretcode(-participant));
  }

  /* Create MBC's own write Topic. */
  topic_write = create_topic(MBC_id, participant);

  /* Create MBC's own Writer. */
  writer = create_writer(participant, topic_write);

  /* Create MBC's membership write Topic. */
  topic_membership = create_membership_topic(participant);

  /* Create MBC's membership Writer. */
  writer_membership = create_membership_writer(participant, topic_membership);
  rc_write_membership = dds_get_instance_handle(writer_membership, &writer_handle);

  /* Create MBC's membership Reader. */
  listener = dds_create_listener(NULL);
  dds_lset_liveliness_changed(listener, on_liveliness_change);
  reader_membership = create_membership_reader(participant, topic_membership, listener);

  /* Build Topic sender id. */
  sprintf(array_convert, "%d", MBC_id);
  strcat(topic_id, " ");
  strcat(topic_id, array_convert);

  /* Announce that this MBC is alive. */
  write_membership_topic(writer_membership, rc_write_membership, MBC_id_32);

  while (!stop)
  {
    total_mbc_added = hash_count();
    dds_get_liveliness_changed_status(reader_membership, &status_members_alive);
    dds_get_publication_matched_status(writer, &status_write);
    if ((add_mbc_liveliness) || ((add_mbc_liveliness == false) && (total_mbc_added < status_members_alive.alive_count)))
    {
      return_status_new_mbc = insert_MBC(reader_membership, writer_membership, MBC_id_32);
      if (return_status_new_mbc)
      {
        read1 = SEARCHING;
        read2 = SEARCHING;
        add_mbc_liveliness = false;
      }
    }
    if (remove_mbc_data)
    {
      delete_MBC(status_members_alive.last_publication_handle);
      read1 = SEARCHING;
      read2 = SEARCHING;
      remove_mbc_data = false;
    }
    if (!add_mbc_liveliness && !remove_mbc_data && (read1 == SEARCHING || read2 == SEARCHING))
    {
      min1 = hash_return_first_min(MBC_id_32);
      min2 = hash_return_second_min(MBC_id_32, min1);
      if (min1 && read1 == SEARCHING)
      {
        topic_read1 = create_topic(min1, participant);
        reader1 = create_reader(participant, topic_read1);
        read1 = READING;
      }
      if (min2 && read2 == SEARCHING)
      {
        topic_read2 = create_topic(min2, participant);
        reader2 = create_reader(participant, topic_read2);
        read2 = READING;
      }
    }
    if (read1 == READING)
    {
      error_read1 = read_topic(reader1, rc_read1, MBC_id);
    }
    if (read2 == READING)
    {
      error_read2 = read_topic(reader2, rc_read2, MBC_id);
    }
    if (status_write.current_count)
    {
      rc_write = write_topic(writer, rc_write, MBC_id, topic_id);
    }
    if (read1 == SEARCHING && read2 == SEARCHING)
    {
      printf("There are no availble MBCs writing\n");
    }
    dds_sleepfor(DDS_MSECS(200));
  }
  /* Deleting the participant will delete all its children recursively as well. */
  rc_read1 = dds_delete(participant);
  if (rc_read1 != DDS_RETCODE_OK)
  {
    DDS_FATAL("dds_delete: %s\n", dds_strretcode(-rc_read1));
  }
  printf("\nExiting safely >> Bye Bye from Matrix Board %d \n", MBC_id);
  return EXIT_SUCCESS;
}

/* Function to handle keyboard interrupt (Ctrl + C). */
void inthand(int signum)
{
  stop = 1;
}

static void on_liveliness_change(dds_entity_t reader, const dds_liveliness_changed_status_t status, void* arg)
{
  (void)arg;
  if (status.alive_count_change > 0)
  {
    add_mbc_liveliness = true;
  }
  else if (status.alive_count_change < 0)
  {
    remove_mbc_data = true;
  }
}

bool insert_MBC(dds_entity_t reader_membership, dds_entity_t writer_membership, int32_t MBC_id_32)
{
  dds_return_t rc_write_membership;
  dds_return_t rc_read_membership;
  struct DataItem* item;
  struct info_member add_member;

  add_member = read_membership_topic(reader_membership, rc_read_membership);
  item = hash_search(add_member.handle);
  if (item == NULL && add_member.new_mbc > 0)
  {
    hash_insert(add_member.handle, add_member.new_mbc);
    dds_sleepfor(DDS_MSECS(200));
    write_membership_topic(writer_membership, rc_write_membership, MBC_id_32);
    return true;
  }
  else
  {
    return false;
  }
}

void delete_MBC(dds_instance_handle_t last_publication_handle)
{
  struct DataItem* item;
  item = hash_search(last_publication_handle);
  if (item != NULL)
  {
    hash_delete(item);
  }
}