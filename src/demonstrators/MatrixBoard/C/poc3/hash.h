#ifndef HASH_H
#define HASH_H

#include "dds/dds.h"
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "MBCData.h"

#define SIZE 10000
struct DataItem
{
  int32_t data;
  dds_instance_handle_t key;
};

struct DataItem* hashArray[SIZE];

dds_instance_handle_t hashCode(dds_instance_handle_t key);
struct DataItem* hash_search(dds_instance_handle_t key);
void hash_insert(dds_instance_handle_t key, int32_t data);
struct DataItem* hash_delete(struct DataItem* item);
void hash_display();
int hash_count();
int hash_return_first_min(int32_t mbc_id);
int hash_return_second_min(int32_t mbc_id, int32_t min1);

#endif