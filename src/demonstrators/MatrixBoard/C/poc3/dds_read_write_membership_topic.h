#ifndef DDS_READ_WRITE_MEMBERSHIP_TOPIC_H
#define DDS_READ_WRITE_MEMBERSHIP_TOPIC_H

/* Define number of Samples read. */
#define MAX_SAMPLES_MEMBERSHIP 1

struct info_member
{
  int32_t new_mbc;
  dds_instance_handle_t handle;
  dds_return_t samples_read;
};

int write_membership_topic(dds_entity_t writer, dds_return_t rc_write, int32_t MBC_id);
struct info_member read_membership_topic(dds_entity_t reader, dds_return_t rc_read);

#endif