#include "MBCData.h"
#include "dds/dds.h"
#include "dds_create_membership_entities.h"
#include "dds_read_write_membership_topic.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* The following function writes the topic using dds_write and prints the contents of the write action. */
int write_membership_topic(dds_entity_t writer, dds_return_t rc_write, int32_t MBC_id)
{
  MBCData_Membership msg;
  /* Create a message to write. */
  msg.mbcID = MBC_id;
  rc_write = dds_write(writer, &msg);
  if (rc_write != DDS_RETCODE_OK)
  {
    DDS_FATAL("dds_write: %s\n", dds_strretcode(-rc_write));
  }
  return rc_write;
}

/* The following function reads the topic using dds_take and prints the contents of the read action. */
struct info_member read_membership_topic(dds_entity_t reader, dds_return_t rc_read)
{
  MBCData_Membership* msg;
  struct info_member return_value;
  dds_sample_info_t infos[MAX_SAMPLES_MEMBERSHIP];
  void* samples[MAX_SAMPLES_MEMBERSHIP];
  samples[0] = MBCData_Membership__alloc();

  return_value.samples_read = dds_take(reader, samples, infos, MAX_SAMPLES_MEMBERSHIP, MAX_SAMPLES_MEMBERSHIP);
  if (return_value.samples_read < 0)
  {
    DDS_FATAL("dds_take: %s\n", dds_strretcode(-return_value.samples_read));
  }
  /* Check if we read some data and it is valid. */
  if ((return_value.samples_read > 0) && (infos[0].valid_data) && (infos[0].instance_state == DDS_IST_ALIVE))
  {
    msg = (MBCData_Membership*)samples[0];
    return_value.new_mbc = msg->mbcID;
    return_value.handle = infos[0].publication_handle;
  }
  MBCData_Membership_free(samples[0], DDS_FREE_ALL);
  return return_value;
}