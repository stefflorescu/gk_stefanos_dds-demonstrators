#include "dds/dds.h"
#include "hash.h"
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

dds_instance_handle_t hashCode(dds_instance_handle_t key)
{
  uint64_t ret;
  ret = key % SIZE;
  return ret;
}

struct DataItem* hash_search(dds_instance_handle_t key)
{
  dds_instance_handle_t hashIndex = hashCode(key);

  while (hashArray[hashIndex] != NULL)
  {
    if (hashArray[hashIndex]->key == key)
    {
      return hashArray[hashIndex];
    }
    ++hashIndex;
    hashIndex %= SIZE;
  }
  return NULL;
}

void hash_insert(dds_instance_handle_t key, int32_t data)
{
  struct DataItem* item = (struct DataItem*)malloc(sizeof(struct DataItem));
  item->data = data;
  item->key = key;
  dds_instance_handle_t hashIndex = hashCode(key);

  while (hashArray[hashIndex] != NULL && hashArray[hashIndex]->key != -1)
  {
    ++hashIndex;
    hashIndex %= SIZE;
  }
  hashArray[hashIndex] = item;
}

struct DataItem* hash_delete(struct DataItem* item)
{
  dds_instance_handle_t key = item->key;
  dds_instance_handle_t hashIndex = hashCode(key);

  while (hashArray[hashIndex] != NULL)
  {
    if (hashArray[hashIndex]->key == key)
    {
      struct DataItem* temp = hashArray[hashIndex];
      hashArray[hashIndex] = NULL;
      return temp;
    }
    ++hashIndex;
    hashIndex %= SIZE;
  }
  return NULL;
}

void hash_display()
{
  int i = 0;
  for (i = 0; i < SIZE; i++)
  {
    if (hashArray[i] != NULL)
    {
      printf(" (%" PRIu64 ",%" PRId32 ")\n", hashArray[i]->key, hashArray[i]->data);
    }
  }
  printf("\n");
}

int hash_count()
{
  int i = 0;
  int count = 0;
  for (i = 0; i < SIZE; i++)
  {
    if (hashArray[i] != NULL)
    {
      count++;
    }
  }
  return count;
}

int hash_return_first_min(int32_t mbc_id)
{
  int32_t min = SIZE;
  int i = 0;
  for (i = 0; i < SIZE; i++)
  {
    if ((hashArray[i] != NULL) && (hashArray[i]->data > mbc_id) && (hashArray[i]->data < min))
    {
      min = hashArray[i]->data;
    }
  }
  if (min == SIZE)
  {
    return 0;
  }
  else
  {
    return min;
  }
}

int hash_return_second_min(int32_t mbc_id, int32_t min1)
{
  int32_t min = SIZE;
  int i = 0;
  for (i = 0; i < SIZE; i++)
  {
    if ((hashArray[i] != NULL) && (hashArray[i]->data > mbc_id) && (hashArray[i]->data < min) &&
        (hashArray[i]->data != min1))
    {
      min = hashArray[i]->data;
    }
  }
  if (min == SIZE)
  {
    return 0;
  }
  else
  {
    return min;
  }
}