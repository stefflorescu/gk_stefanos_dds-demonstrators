#include <chrono>
#include <iostream>
#include <thread>

#include "roundtrip.hpp"

void errorMessage();

/**
 * @brief main contains the main code for the roundtrip project
 *
 * @param argc number of arguments
 * @param argv the arguments of the function
 * @return int endstate of the application
 */
int main(int argc, char const *argv[]) {
    if (argc != 4) {
        errorMessage();
        return -1;
    }
    unsigned int id;
    unsigned int totalDevices;
    unsigned int totalRoundtrips;
    try {
        id = std::stoul(argv[1]);
        totalDevices = std::stoul(argv[2]);
        totalRoundtrips = std::stoul(argv[3]);
        if (id > totalDevices || totalDevices <= 1) {
            errorMessage();
            return -1;
        }
    } catch (const std::exception &e) {
        errorMessage();
        return -1;
    }
    RoundTrip roundtrip(id, totalDevices, totalRoundtrips);
    roundtrip.run();

    const auto totalRoundtripTime = roundtrip.getTime();

    if (id != 1) {
        std::cout << "Roundtrip finished, see device 1 for measurements\n\n";
        return 0;
    }

    std::cout << "RoundTrip finished!\n"
              << "Total execution time for " << totalRoundtrips << " roundtrips: " << totalRoundtripTime
              << " microseconds\n"
              << "Average time per roundtrip: " << totalRoundtripTime / static_cast<double>(totalRoundtrips)
              << " microseconds\n"
              << "Time between each device within the roundtrip: "
              << (totalRoundtripTime / static_cast<double>(totalRoundtrips)) / totalDevices << " microseconds\n\n";

    return 0;
}

/**
 * @brief errorMessage contains the fault message for wrong parameters
 *
 */
void errorMessage() {
    std::cerr
        << "Wrong input for this application!\n\n"
        << "Format: ./RoundTrip <device ID> <Total devices> <Round trips>\n\n"
        << "<device ID>: The ID of the device within the round trip, must be unique, > 0 and <= total devices\n"
        << "<Total devices>: The amount of devices in the round trip loop (the amount of roundtrip programs running)\n"
        << "<Round Trips>: Amount of round trips to execute\n\n"
        << "Note: Only the <device ID> is unique! The other 2 parameters MUST be equal for each roundtrip program!\n"
        << "Note: Device ID 1 initiates the roundtrip. Therefore, device ID 1 must be started last!\n"
        << "Note: <Round Trips> should be equal for all applications";
}