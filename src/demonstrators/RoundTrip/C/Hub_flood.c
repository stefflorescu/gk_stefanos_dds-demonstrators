#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include "dds/dds.h"
#include "HubData.h"

/* Define max number of MBCs. */
#define MAX_SAMPLES 1

volatile sig_atomic_t stop;

/* Function to handle keyboard interrupt (Ctrl + C). */
void inthand(int signum){
  stop = 1;
}

int main (int argc, char *argv[]){

  dds_entity_t participant;
  dds_entity_t topic_write;
  dds_entity_t topic_read;
  dds_entity_t reader;
  dds_return_t rc_read;
  dds_entity_t writer;
  HubData_Msg msg;
  HubData_Msg *msg1;
  void *samples[MAX_SAMPLES];
  dds_sample_info_t infos[MAX_SAMPLES];
  dds_return_t rc_write;
  dds_qos_t *qos;
  dds_publication_matched_status_t status_write;
  dds_subscription_matched_status_t status_read;
  uint32_t status = 0;
  dds_status_id_t test;
  bool send_state = false;

  char buf = argv[1][6];
  int Hub_id = buf -'0';

  /* Create a Participant.*/
  participant = dds_create_participant (DDS_DOMAIN_DEFAULT, NULL, NULL);
  if (participant < 0){
   DDS_FATAL("dds_create_participant: %s\n", dds_strretcode(-participant));
  }

  topic_write = dds_create_topic (participant, &HubData_Msg_desc, argv[2], NULL, NULL);
  if (topic_write < 0){
    DDS_FATAL("dds_create_topic: %s\n", dds_strretcode(-topic_write));
  }

  topic_read = dds_create_topic (participant, &HubData_Msg_desc, argv[1], NULL, NULL);
  if (topic_read < 0){
    DDS_FATAL("dds_create_topic: %s\n", dds_strretcode(-topic_read));
  }

  signal(SIGINT, inthand);

  if (argc != 3){
    printf("Two arguments are expected.\n");
    return EXIT_FAILURE;
  }

  /* Create a Writer. */
  writer = dds_create_writer (participant, topic_write, NULL, NULL);
  if (writer < 0){
    DDS_FATAL("dds_create_write: %s\n", dds_strretcode(-writer));
  }

  /* Create a reliable Reader. */
  qos = dds_create_qos ();
  dds_qset_reliability (qos, DDS_RELIABILITY_RELIABLE, DDS_SECS (10));
  reader = dds_create_reader (participant, topic_read, qos, NULL);
  if (reader < 0){
    DDS_FATAL("dds_create_reader: %s\n", dds_strretcode(-reader));
  }
  dds_delete_qos(qos);

  rc_write = dds_set_status_mask(writer, DDS_PUBLICATION_MATCHED_STATUS);
  if (rc_write != DDS_RETCODE_OK){
    DDS_FATAL("dds_set_status_mask: %s\n", dds_strretcode(-rc_write));
  }

  rc_read = dds_set_status_mask(reader, DDS_SUBSCRIPTION_MATCHED_STATUS);
  if (rc_read != DDS_RETCODE_OK){
    DDS_FATAL("dds_set_status_mask: %s\n", dds_strretcode(-rc_read));
  }

  samples[0] = HubData_Msg__alloc ();

  while(!stop){
    rc_read = dds_get_subscription_matched_status (reader, &status_read);
    //printf("Number of active writers on topic_read: %"PRIu32"\n",status_read.current_count);
    /* Check if we have active writers to our read topic. */
    if (status_read.current_count){
      rc_read = dds_take (reader, samples, infos, MAX_SAMPLES, MAX_SAMPLES);
      if (rc_read < 0){
        DDS_FATAL("dds_read: %s\n", dds_strretcode(-rc_read));
      }
      msg1 = (HubData_Msg*) samples[0];
      /* Check if we read some data and it is valid. */
      if ((rc_read > 0) && (infos[0].valid_data)){
        /* Print Message. */
        //printf ("=== [Hub %d] Received : %u\n", Hub_id, msg1->userID);
        fflush (stdout);
        send_state = true;
      }
    }
    rc_write = dds_get_publication_matched_status (writer, &status_write);
    //printf("Number of active readers on topic_write: %"PRIu32"\n",status_write.current_count);
    /* Check if we have active readers to our write topic. */
    if (status_write.current_count && send_state){
      /*Create a message to write. */
      msg.userID = msg1->userID + 1;
      //printf ("=== [Hub %d]  Writing : %u\n" ,Hub_id, msg.userID);
      rc_write = dds_write (writer, &msg);
      if (rc_write != DDS_RETCODE_OK){
        DDS_FATAL("dds_write: %s\n", dds_strretcode(-rc_write));
      }
      send_state = false;
    }
    /* Polling sleep mostly for testing and debugging. */
    //dds_sleepfor (DDS_MSECS (1000));
  }

  HubData_Msg_free (samples[0], DDS_FREE_ALL);
  /* Deleting the participant will delete all its children recursively as well. */
  rc_read = dds_delete (participant);
  if (rc_read != DDS_RETCODE_OK){
    DDS_FATAL("dds_delete: %s\n", dds_strretcode(-rc_read));
  }

  return EXIT_SUCCESS;
}
