#!/usr/bin/env python3

import zmq

import sys


receive_url =sys.argv[1]
send_url = sys.argv[2]

def main():
    

    print("%s -> %s" % (receive_url, send_url))
    
    context = zmq.Context()
    sender = context.socket(zmq.PAIR)
    sender.connect(receive_url)
    
    context = zmq.Context()
    receiver = context.socket(zmq.PAIR)                              # XXX GAM: hardcoded, use also other network-patterns
    receiver.bind(send_url)
               
    while True:
        msg = receiver.recv().decode()
        sender.send_string(msg)
    pass



if __name__ == "__main__":
    main()
