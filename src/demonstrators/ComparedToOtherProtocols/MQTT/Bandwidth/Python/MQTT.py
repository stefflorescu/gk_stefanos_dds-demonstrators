#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime
from time import sleep
import sys

import logging
logging.basicConfig(level=logging.INFO)
from logging import getLogger
logger = getLogger(__name__)

ReveiceTopic =  sys.argv[1]      if len(sys.argv) >1 else 'hub'
SendTopic =     sys.argv[2]      if len(sys.argv) >2 else 'master'
SERVER =        sys.argv[3]      if len(sys.argv) >3 else 'localhost'
ISMASTER =      int(sys.argv[4]) if len(sys.argv) >4 else 0
QOS =           int(sys.argv[5]) if len(sys.argv) >5 else 0

if(ISMASTER):
    logger.info("the master is started with server:")
else:
    logger.info("a hub is started with server:")    
logger.info(SERVER)
logger.info(QOS)

EVERY=3000
number_of_messages_to_send = 10000
which_messages_received = [0]*number_of_messages_to_send


t0, t1 = None, None




def send_message(client, counter):
    client.publish(SendTopic, counter, QOS)
    logger.debug("ping client: %s, counter=%i", client, counter)
    logger.debug("sending on topic: %s " % SendTopic)

def on_data_received(client, userdata, message):
    message_number = int(message.payload.decode().split()[0])
    logger.debug("pong  %i", message_number)
    if(ISMASTER):
        which_messages_received[message_number] = 1
        if(which_messages_received.count(1)%EVERY==0):
            print("jep")
            report(which_messages_received.count(1))
    else:
        send_message(client, message_number)

def on_connect(client, userdata, flags, rc):
    client.subscribe(ReveiceTopic, QOS)
    logger.debug("Subscribed to %s", ReveiceTopic)

def report(messages_up_until_now):
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    # print("PingPong already %s messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (c, t, c/t, t*1000/c))
    logger.info("it took %s messages %s seconds that is %s ms/msg" %(messages_up_until_now, t,t/messages_up_until_now*1000))
 
    

def send_message_bunch(client, number_of_messages):
    messages =[]
        
    for i in range(number_of_messages):
        messages.append({'topic':SendTopic, 'payload':i, 'qos':QOS})
        
    publish.multiple(messages, hostname="localhost")




client = mqtt.Client()
client.on_connect = on_connect # with 1st ping
client.on_message = on_data_received





client.connect(SERVER)
client.loop_start() # in thread

if(ISMASTER):
    logger.info("sending all messages")
    global t0
    t0 = datetime.now()
    send_message_bunch(client, number_of_messages_to_send)
    
    while 0 in which_messages_received:
        sleep(1)
        
    report(number_of_messages_to_send)
    print("done")
else:    
    while True:
        sleep(1)  
  
client.loop_stop()



