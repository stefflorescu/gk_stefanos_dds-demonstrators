#!/usr/bin/env python3

import paho.mqtt.client as mqtt
from datetime import datetime
from time import sleep

SERVER = ['localhost']

EVERY=1000*10*3
TOPIC  = "GAM/PingPong"

t0, t1 = None, None

import logging
logging.basicConfig(level=logging.INFO)
from logging import getLogger
logger = getLogger(__name__)


def ping(client, counter):
    client.publish(TOPIC, counter)
    logger.debug("ping client: %s, counter=%i", client, counter)

def on_pong(client, userdata, message):
    count = int(message.payload.decode().split()[0])
    logger.debug("pong  %i", count)
    ping(client, count+1)
    if count % EVERY == 0:
        report(count)

def on_connect(client, userdata, flags, rc):
    global t0
    t0 = datetime.now()
    client.subscribe(TOPIC)
    logger.debug("Subscribed to %s", TOPIC)
    ping(client, 1) # start counting
    logger.info("Started")

def report(c):
    global t1
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("PingPong already %s messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (c, t, c/t, t*1000/c))


def stop(c):
    global t1
    t1 = datetime.now()
    client.loop_stop()
    print("Stopped at:", t1)
    t = (t1-t0 ).total_seconds()
    print("PingPong %s messages, in %f seconds: %f msg/sec" % (c, t, c/t))
    quit()

client = mqtt.Client()
client.on_connect = on_connect # with 1st ping
client.on_message = on_pong


client.connect(*SERVER)
client.loop_start() # in thread

sleep(60*60*24)
client.loop_stop()



